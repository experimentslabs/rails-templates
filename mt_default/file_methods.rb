# frozen_string_literal: true

def relax_ruby_version
  gsub_file 'Gemfile', "ruby '#{RUBY_VERSION}'", "ruby '~>#{RUBY_VERSION}'"
  lint_and_commit 'Relax ruby version in Gemfile', '.', false
end

def create_editorconfig
  # FIXME: Add ruby files
  # FIXME: Only add JS/VUE/SCSS files when not in API mode
  copy_template ['.editorconfig']
  lint_and_commit 'Create .editorconfig with basic configuration', '.', false
end

def create_changelog
  doc :dev do
    <<~MD
      ## Change log

      Check [CHANGELOG.md](CHANGELOG.md) for the changes that are integrated
      to the project. Each branch has a changelog corresponding to its state.

      ## Roadmap

      Check [ROADMAP.md](ROADMAP.md) to see the planned changes. It only lists
      upcoming features and may not be followed to the letter.
    MD
  end
  copy_template ['CHANGELOG.md']
  copy_template ['ROADMAP.md']
  lint_and_commit 'Add Roadmap and Changelog', '.', false
end
