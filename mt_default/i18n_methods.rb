# frozen_string_literal: true

def _add_rails_i18n
  inject_into_file 'Gemfile', after: 'group :development, :test do' do
    "\n  gem 'rails-i18n'"
  end
end

def _create_locale_initializer
  copy_template ['config', 'initializers', 'locale.rb']
  lint_and_commit "Add I18n configuration (#{@mt_config[:locales].join ', '})"
end

def _add_i18n_tasks
  inject_into_file 'Gemfile', after: 'group :development, :test do' do
    "\n  gem 'i18n-tasks'"
  end
  bundle_install
  run 'cp $(i18n-tasks gem-path)/templates/config/i18n-tasks.yml config/'
  run 'cp $(i18n-tasks gem-path)/templates/rspec/i18n_spec.rb spec/'

  gsub_file 'spec/i18n_spec.rb', "RSpec.describe 'I18n' do", "RSpec.describe 'I18n task' do # rubocop:disable RSpec/DescribeClass"

  gsub_file 'config/i18n-tasks.yml', 'base_locale: en', "base_locale: #{@mt_config[:default_locale]}"
  lint_and_commit 'Add I18n-tasks'
end

def _add_devise_i18n
  # We need DB access to generate this, so let's hardcode it
  run 'mkdir app/i18n'
  copy_template ['app', 'i18n', 'model_attributes.i18n']

  inject_into_file 'Gemfile', after: @mt_config[:app_gems_separator] do
    "\ngem 'devise-i18n'"
  end
  bundle_install
  lint_and_commit 'Add Devise-i18n'
end

def _create_i18n_add_missing_task
  # Runner to generate attributes
  copy_template ['lib', 'runners', 'generate_i18n_model_attributes.rb']
  lint_and_commit 'Create runner to generate translation strings for models'

  # i18n:add-missing task
  copy_template ['lib', 'tasks', 'i18n.rake']
  lint_and_commit 'Create task "i18n:add-missing" which prefixes missing strings'
end

def _add_i18n_js # rubocop:disable Metrics/AbcSize
  inject_into_file 'Gemfile', after: @mt_config[:app_gems_separator] do
    "\ngem 'i18n-js'"
  end

  inject_into_file 'app/views/layouts/application.html.erb', after: /<%= javascript_include_tag 'application'(, 'data-turbolinks-track': 'reload')? %>/ do
    render_template ['views', 'layouts', 'application', 'i18n-js.html']
  end

  inject_into_file 'config/application.rb', after: '# the framework and any gems in your application.' do
    <<~RB

      # Generate JS translations
      config.middleware.use I18n::JS::Middleware
    RB
  end

  inject_into_file 'app/javascript/packs/application.js', after: "// that code so it'll be compiled." do
    <<~JS

      import I18n from '../../../tmp/i18n'

      window.I18n = I18n
    JS
  end

  run 'mkdir app/javascript/packs/locales/'
  run 'touch app/javascript/packs/locales/.keep'

  append_file '.gitignore' do
    <<~IGNORE
      /app/javascript/packs/locales/*
      !/app/javascript/packs/locales/.keep
    IGNORE
  end

  copy_template ['config', 'i18n-js.yml']
  lint_and_commit 'Add i18n-js'

  return unless @mt_config[:use_vue]

  copy_template ['lib', 'vue_i18n_scanner.rb']

  inject_into_file 'config/i18n-tasks.yml', after: '# i18n-tasks finds and manages missing and unused translations: https://github.com/glebm/i18n-tasks' do
    "\n# Custom scanner for VueJS templates\n<% require './lib/vue_i18n_scanner' %>"
  end
  lint_and_commit 'I18n-tasks: Add a scanner for Vue files'
end

def internationalize
  doc :dev do # rubocop:disable Metrics/BlockLength
    content = <<~MD
      ### Internationalization

      Internationalization is made as in "traditional" Rails applications, and
      is managed with [i18n-tasks](https://github.com/glebm/i18n-tasks)

      `i18n-tasks` helps to check if your translations are up-to-date.

      A custom rake task exists to add missing translations, prefixed with
      `TRANSLATE_ME` to find them easily, and another one adds the model
      attributes in a dummy file, so they can be translated.

      ```sh
      # Check
      i18n-tasks health
      # Add missing model attributes in `app/i18n/model_attributes.i18n`
      rake i18n:add-models-attributes
      # Add missing translations
      rake i18n:add-missing
      # Remove unused translations
      i18n-tasks remove-unused
      ```

      Check [config/i18n-tasks.yml](config/i18n-tasks.yml) for configuration.

      An RSpec test checks for missing/unused translations, and a CI job tests
      for missing model attributes translations.
    MD

    if @mt_config[:use_i18njs]
      content += <<~MD

        #### Javascript

        Thanks to [i18n-js](http://rubygems.org/gems/i18n-js), scripts can use
        an `I18n` global to translate strings.

        The only keys exported are `js.*` and `generic.*`. Check
        [config/i18n-js.yml](config/i18n-js.yml) for configuration.
      MD
    end

    content
  end

  _add_rails_i18n
  _create_locale_initializer
  _add_i18n_tasks
  _create_i18n_add_missing_task
  _add_i18n_js if @mt_config[:use_i18njs]
end

def _cleanup_translations
  run 'bundle exec i18n-tasks remove-unused -y'
  lint_and_commit 'Remove unused translations'

  run 'bundle exec rails i18n:add-missing'

  run 'bundle exec i18n-tasks normalize -p'
  lint_and_commit "Add ActiveRecord strings for #{@mt_config[:auth_singular]} model"
end

def create_translations
  _cleanup_translations

  return unless Dir[File.join('config', 'locales', '**', '*')].count { |file| File.file?(file) }.zero?

  @mt_config[:locales].each do |locale|
    copy_template ['config', 'locales', 'locale.yml'], "#{locale}.yml"
  end
  lint_and_commit 'Keep "config/locales"'
end
