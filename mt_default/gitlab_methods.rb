# frozen_string_literal: true

def create_gitlab_ci
  doc :dev do
    <<~MD
      ### Continuous integration

      CI jobs are configured for Gitlab. Check
      `[.gitlab-ci.yml](.gitlab-ci.yml)` to see the list.
    MD
  end
  copy_template ['.gitlab-ci.yml']
  lint_and_commit 'Configure Gitlab CI', '.', false
end

def create_gitlab_templates
  run 'mkdir -p .gitlab/merge_request_templates/'

  copy_template ['.gitlab', 'merge_request_templates', 'Merge request.md']
  lint_and_commit 'Add Gitlab request template', '.', false
end
