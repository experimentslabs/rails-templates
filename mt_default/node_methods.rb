# frozen_string_literal: true

def _complete_node_gitignore
  append_file '.gitignore' do
    <<~GITIGNORE
      # Node dependencies
      /node_modules
      /yarn-error.log
      yarn-debug.log*
      .yarn-integrity

    GITIGNORE
  end
end

def create_node_version
  if @mt_config[:node_version]
    file '.node-version' do
      @mt_config[:node_version].split('.').first.sub('v', '')
    end
  else
    puts "\n\nNODE NOT AVAILABLE, ESLINT AND STYLELINT WILL NOT BE INSTALLED\n\n"
  end

  _complete_node_gitignore

  lint_and_commit 'Add .node-version and ignore node-related files', '.', false
end

def _add_eslint
  # Inject things first, file will be properly reformatted when running "yarn add"
  inject_into_file 'package.json', after: "\"private\": true,\n" do
    <<~JSON
      "scripts": {
        "lint:js": "eslint app/#{@mt_config[:use_webpacker] ? 'javascript' : 'assets/javascripts'}/**/*.js",
        "lint:js-fix": "eslint app/#{@mt_config[:use_webpacker] ? 'javascript' : 'assets/javascripts'}/**/*.js --fix"
      },
    JSON
  end
  run 'yarn add --dev eslint eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard'
  run 'yarn add --dev eslint-plugin-vue' if @mt_config[:use_vue]

  copy_template ['.eslintrc.yml']

  lint_and_commit 'Add Eslint', '.', false
end

def setup_eslint
  doc :test do
    content = <<~MD
      ### ESLint - [site](https://github.com/eslint/eslint)

      ESLint is configured to use Standard configuration and some small
      tweaks.
    MD

    if @mt_config[:use_vue]
      content += <<~MD

        Configuration for VueJS is based on the `vue/recommended` plugin.
      MD
    end

    content += <<~MD

      Run ESLint with:
      ```sh
      yarn run lint:js
      # To auto-fix files:
      yarn run lint:js-fix
      ```
    MD

    content
  end

  _add_eslint

  run 'yarn run lint:js-fix'

  lint_and_commit 'Lint JS files', '.', false
end

def setup_stylelint
  doc :test do
    <<~MD
      ### StyleLint - [site](https://github.com/stylelint/stylelint)

      Stylelint is configured to use the
      [stylelint-scss](https://github.com/kristerkari/stylelint-scss) plugin;
      the
      [stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard)
      and
      [stylelint-config-recommended-scss](https://github.com/kristerkari/stylelint-config-recommended-scss)
      configurations.

      ```sh
      yarn lint:sass
      ```

      **Note:** don't even try to auto-fix your files, auto-fixing ignores the
      "disabling" directives in your files.
    MD
  end

  # Inject things first, file will be properly reformatted when running "yarn add"
  inject_into_file 'package.json', after: "\"scripts\": {\n" do
    <<~JSON
      "lint:sass": "stylelint app/assets/stylesheets",
    JSON
  end
  run 'yarn add --dev stylelint stylelint-scss stylelint-config-recommended-scss stylelint-config-standard'
  copy_template ['.stylelintrc.yml']

  lint_and_commit 'Add Stylelint', '.', false
end

def _create_vue_directories
  copy_template ['app', 'javascript', 'vue', 'helpers', '.keep']
  copy_template ['app', 'javascript', 'vue', 'mixins', '.keep']
  copy_template ['app', 'javascript', 'vue', 'stores', '.keep']
  copy_template ['app', 'javascript', 'vue', 'views', '.keep']

  lint_and_commit 'Create directories for VueJS files', '.', false
end

def _add_vue_i18n
  run 'yarn add vue-i18n'
  copy_template ['app', 'javascript', 'vue', 'helpers', 'i18n.js']
  run 'rm app/javascript/vue/helpers/.keep'

  lint_and_commit 'VueJS: Create helper for i18n', '.', false
end

def setup_vuejs
  doc :dev do
    <<~MD
      ### VueJS

      This project uses VueJS.

      The VueJS files should be organized this way:

      ```text
      app/javascript/
      ├── packs/
      │   ├── vue_app1
      │   ├── vue_app2
      │   └── ...
      └── vue/
          ├── helpers/
          ├── mixins/
          ├── stores/ # VueX stores (use modules to reuse stores parts across apps)
          └── views/  # For the views, use the same organization as a Rails app:
              └── posts/
                  ├── _form.vue
                  ├── _post.vue
                  ├── index.vue
                  ├── show.vue
                  └── ...
      ```
    MD
  end
  _create_vue_directories

  _add_vue_i18n if @mt_config[:internationalize]
end
