# frozen_string_literal: true

TEMPLATES_DIR = File.join(__dir__, 'templates')
PARTIALS_DIR  = File.join(__dir__, 'partials')

RAILS_VERSION = Gem::Version.new Rails.version

RAILS_5 = Gem::Version.new('5.0.0')
RAILS_6 = Gem::Version.new('6.0.0')
RAILS_7 = Gem::Version.new('7.0.0')

IS_RAILS_5 = RAILS_VERSION >= RAILS_5 && RAILS_VERSION < RAILS_6
IS_RAILS_6 = RAILS_VERSION >= RAILS_6 && RAILS_VERSION < RAILS_7

CONFIG_FILE = File.join Dir.home, '.rails_templates_mt_defaults.yml'

def save_config(config)
  File.write CONFIG_FILE, config.to_yaml
end

def load_config
  YAML.load_file CONFIG_FILE if File.exist? CONFIG_FILE
end

def copy_template(dest, custom_output_name = nil)
  template_path = dest.clone
  template_path[template_path.size - 1] += '.erb'
  dest[dest.size - 1] = custom_output_name if custom_output_name
  template File.join(TEMPLATES_DIR, template_path), File.join(dest), force: true
end

def render_template(path)
  partial_path = path.clone
  partial_path[partial_path.size - 1] += '.erb'
  erb = ERB.new File.read File.join(PARTIALS_DIR, partial_path)
  erb.result(binding)
end

def lint_and_commit(message, files = '.', lint = true)
  run 'bundle exec rubocop -a', capture: true if lint
  git add: files
  git commit: "-m \"Initial: #{message.gsub(/"/, '\\"')}\""
end

def bundle_install
  run 'bundle install', capture: true
end

def rails_6?
  IS_RAILS_6
end

def rails_5?
  IS_RAILS_5
end

def question_user(rails_options: {}) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  if File.exist? CONFIG_FILE
    return load_config unless no?('Use last config? (Y/n)')
  end
  config = {
    use_postgres: rails_options['database'] == 'postgresql',
    is_api: rails_options['api'],
    dev_test_gems_separator: 'group :development, :test do',
    test_gems_separator: 'group :development, :test do', # 'group :test do',
    app_gems_separator: '# App specific gems',
  }

  config[:internationalize] = !no?('Internationalize the app? (Y/n)')
  if config[:internationalize]
    default_locale          = ask 'Default locale (leave empty for "en")? (i.e.: fr)'
    config[:default_locale] = default_locale != '' ? default_locale.to_sym : :en
    locales                 = ask 'Additional locales? (i.e.: fr ru)'
    locales                 = locales.split(' ').map(&:to_sym)
    config[:locales]        = locales.unshift config[:default_locale]
    config[:use_i18njs]     = config[:is_api] ? false : !no?('Use I18n-js for JS translations? (Y/n)')
  end

  config[:use_devise] = !no?('Use Devise? (Y/n)')
  if config[:use_devise]
    auth_table             = ''
    auth_table             = ask 'Devise table (leave empty for "users")? ' if config[:use_devise]
    config[:auth_table]    = auth_table != '' ? auth_table : 'users'
    config[:auth_model]    = "app/models/#{config[:auth_table].singularize}.rb"
    config[:auth_singular] = config[:auth_table].singularize
    config[:auth_username] = yes?('Do you want usernames? (y/N)')

    config[:devise_copy_translations] = config[:internationalize] ? yes?('Copy devise translation? (y/N)') : false
    config[:devise_copy_views]        = yes?('Copy devise views? (y/N)')

    config[:use_pundit] = yes?('Do you need authorization management? (y/N)')
    config[:use_roles] = yes?('Do you want roles (with default support for admin/user)? (y/N)')
    config[:create_member_namespace] = !no?('Create a member application controller? (Y/n)')

    config[:create_admin_namespace] = !no?('Create an admin application controller? (Y/n)') if config[:use_roles]

    config[:setup_controller_namespaces] = config[:create_member_namespace] || config[:create_admin_namespace]
  end

  config[:create_changelog] = !no?('Create a changelog and roadmap file? (Y/n)')

  # Only ask if not explicitly skipped
  config[:has_tests] = if rails_options[:skip_test] == false
                         !no?("Install RSpec#{rails_options[:api] ? '' : ' and Cucumber'}? (Y/n)")
                       else
                         false
                       end
  config[:use_rspec_rails_api] = config[:has_tests] ? !no?("Install RSpec Rails API to test your API responses and create swagger\n    documentation? (Y/n)") : false

  # Only ask if tests are disabled
  config[:use_factorybot] = if config[:has_tests]
                              true
                            else
                              !no?('Well... Install FactoryBot and Faker? (Y/n)')
                            end

  config[:use_webpacker] = !rails_options[:skip_webpack_install]
  config[:use_vue]       = rails_options[:webpack] == 'vue'

  node_version          = `node --version`
  config[:node_version] = $?.success? ? node_version : nil # rubocop:disable Style/SpecialGlobalVars

  save_config config unless no?('Save config? (Y/n)')

  config
end

def doc(section)
  @doc ||= {
    intro: ["# Readme\n"],
    setup: ["## Getting started\n"],
    dev: ["## Development\n"],
    test: ["## Testing\n"],
  }
  raise 'No documentation given' unless block_given?

  @doc[section] ||= []
  @doc[section].push(yield)
end

def add_gemfile_separator
  inject_into_file 'Gemfile', after: /gem 'bootsnap', '.*', require: false/ do
    "\n\n#{@mt_config[:app_gems_separator]}"
  end
end

def convert_to_scss
  doc :dev do
    <<~MD
      ### Style

      Stylesheets are located in `app/assets/stylesheets/`, as usual. We use
      SCSS language.
    MD
  end
  run 'mv app/assets/stylesheets/application.css app/assets/stylesheets/application.scss'
  # Don't require everything
  gsub_file 'app/assets/stylesheets/application.scss', / \*= require.*\n/, ''
  lint_and_commit 'Change application.css', '.', false
end

def fancy_line(content, length = 80)
  line = "\n║ #{content}"
  "#{line}#{' ' * (length - line.length)}║"
end

def configure_action_mailer
  # FIXME: Inject this at the right place.
  environment "config.action_mailer.default_url_options = { host: 'http://localhost', port: 3000 }", env: 'development'
  environment "config.action_mailer.default_url_options = { host: 'http://localhost' }", env: 'test'

  lint_and_commit 'Add ActionMailer configuration', '.'
end
