# frozen_string_literal: true

RAILS6_RSPEC_GEMS = <<~'TEXT'
  %w[rspec-core rspec-expectations rspec-mocks rspec-rails rspec-support].each do |lib|
    gem lib, git: "https://github.com/rspec/#{lib}.git", branch: 'main'
  end
TEXT
def setup_rubocop
  doc :test do
    <<~MD
      ### Rubocop - [site](https://rubocop.org/)

      Rubocop checks for coding standards, allowing us to have consistent
      coding style in the project. Configuration is in
      [.rubocop.yml](.rubocop.yml).

      Enabled plugins:

      - [rubocop-performance](https://docs.rubocop.org/projects/performance),
      - [rubocop-rails](https://docs.rubocop.org/projects/rails/) for common
        errors in Rails projects
      - [rubocop-rspec](https://github.com/rubocop-hq/rubocop-rspec)

      Run it with:

      ```sh
      bundle exec rubocop
      # To fix automatically what can be fixed:
      bundle exec rubocop -a
      ```
    MD
  end

  inject_into_file 'Gemfile', after: @mt_config[:dev_test_gems_separator] do
    "\n  gem 'rubocop', require: false\n  gem 'rubocop-performance'\n  gem 'rubocop-rails'#{@mt_config[:has_tests] ? "\n  gem 'rubocop-rspec'" : ''}"
  end
  bundle_install

  copy_template ['.rubocop.yml']
  lint_and_commit "Add Rubocop, with plugins performance, rails#{@mt_config[:has_tests] ? ' and rspec' : ''}", '.', false

  # Fix everything
  gsub_file 'config/puma.rb', 'fetch("RAILS_MAX_THREADS") { 5 }', "fetch('RAILS_MAX_THREADS', 5)"
  gsub_file 'config/puma.rb', 'fetch("PORT") { 3000 }', "fetch('PORT', 3000)"
  gsub_file 'config/environments/production.rb', 'ActiveSupport::Logger.new(STDOUT)', 'ActiveSupport::Logger.new($stdout)'
  lint_and_commit 'Make Rubocop happy'
end

def setup_database_cleaner
  inject_into_file 'Gemfile', after: @mt_config[:test_gems_separator] do
    "\n  gem 'database_cleaner'"
  end

  lint_and_commit 'Add Database cleaner'
end

def _add_rspec
  inject_into_file 'Gemfile', after: @mt_config[:dev_test_gems_separator] do
    if rails_6?
      RAILS6_RSPEC_GEMS
    else
      "\n  gem 'rspec-rails', '~> 3.8'"
    end
  end

  bundle_install
  lint_and_commit 'Add RSpec'
end

def _configure_rspec
  generate 'rspec:install'

  # Enable all of the disabled "but recommended" options
  gsub_file 'spec/spec_helper.rb', /=begin\n/, "\n"
  gsub_file 'spec/spec_helper.rb', /=end\n/, ''

  # Save failing examples in tmp/
  gsub_file 'spec/spec_helper.rb', 'spec/examples.txt', 'tmp/rspec_example_status.txt'

  append_file '.rspec', "\n--format documentation"

  inject_into_file 'spec/rails_helper.rb', after: '# config.filter_gems_from_backtrace("gem name")' do
    "\n\nconfig.after(:suite) do\n  DatabaseCleaner.clean_with(:truncation)\nend"
  end

  lint_and_commit 'Configure RSpec'
end

def _add_rspec_rails_api
  inject_into_file 'Gemfile', after: @mt_config[:test_gems_separator] do
    "\n  gem 'rspec-rails-api'"
  end
  bundle_install

  inject_into_file 'spec/rails_helper.rb', before: 'RSpec.configure do |config|' do
    "\nRSpec::Rails::DIRECTORY_MAPPINGS[:acceptance] = %w[spec acceptance]\n\n"
  end
  inject_into_file 'spec/rails_helper.rb', before: /end\n\z/ do
    "\nconfig.include RSpec::Rails::RequestExampleGroup, type: :acceptance\n"
  end
  inject_into_file '.rubocop.yml', before: 'RSpec/NestedGroups:' do
    <<~YAML
      RSpec/EmptyExampleGroup:
        CustomIncludeMethods:
          - for_code

    YAML
  end

  copy_template ['spec', 'acceptance_helper.rb']

  run 'mkdir spec/acceptance'
  run 'touch spec/acceptance/.keep'

  lint_and_commit 'Add RSpec Rails API'
end

def setup_rspec
  doc :test do # rubocop:disable Metrics/BlockLength
    content = <<~MD
      ### RSpec - [site](https://github.com/rspec/rspec)

      RSpec examples are in `spec/`. Run the suite with:

      ```sh
      bundle exec rspec
      ```

      To debug step by step:
      ```sh
      # Run this once
      bundle exec rspec
      # Then run this to replay failed examples
      bundle exec rspec --only-failures
      ```
    MD

    if @mt_config[:use_rspec_rails_api]
      content += <<~MD

        #### Acceptance tests

        JSON responses are tested with
        [rspec-rails-api](https://gitlab.com/experimentslabs/rspec-rails-api).

        This gem can generate Swagger documentation, but it's not enabled on
        this project.
      MD
    end

    if @mt_config[:use_devise]
      content += <<~MD

        #### Shared contexts

        As the project uses Devise for authentication, some shared contexts are
        available to use in the specs:

        - 'with authenticated user'#{@mt_config[:use_roles] ? "\n- 'with authenticated admin'" : ''}
      MD
    end

    content
  end

  _add_rspec
  _configure_rspec
  _add_rspec_rails_api if @mt_config[:use_rspec_rails_api]
end

def setup_factorybot
  doc :dev do
    <<~MD
      ### FactoryBot - [site](https://github.com/thoughtbot/factory_bot)

      FactoryBot Rails allows to quickly create entities. It's useful in
      tests, as creating fixtures can be a real pain.

      They are located in `spec/factories`, and are available in Rails
      console, RSpec and Cucumber. It's also used for development seeds.

      Note that FactoryBot is not available in production.

      FactoryBot is configured to create related entities by default, instead
      of only building them (`FactoryBot.use_parent_strategy = false`)

      #### Check the factories

      A rake task can be used to test the ability to run a factory multiple
      times:

      ```sh
      rake factory_bot:lint
      ```

      This task is executed in CI

      ### Faker - [site](https://github.com/faker-ruby/faker)

      Faker is used during development to generate fake data. It should be
      used in new factories.
    MD
  end

  inject_into_file 'Gemfile', after: @mt_config[:dev_test_gems_separator] do
    "\n  gem 'factory_bot_rails'\n  gem 'faker'"
  end
  bundle_install
  lint_and_commit 'Add FactoryBot and Faker'

  inject_into_file 'spec/rails_helper.rb', before: '# Requires supporting ruby files with custom matchers and macros, etc, in' do
    "\n  FactoryBot.use_parent_strategy = false\n"
  end
  lint_and_commit "FactoryBot: Don't use parent strategy"

  # FactoryBot-lint task
  copy_template ['lib', 'tasks', 'factory_bot.rake']
  lint_and_commit 'Create "factory_bot:lint" task to check factories'
end

def _add_cucumber
  inject_into_file 'Gemfile', after: @mt_config[:test_gems_separator] do
    "\n  gem 'cucumber-rails', require: false"
  end
  bundle_install
  generate 'cucumber:install'

  _fix_cucumber_task

  run 'cp config/database.yml config/database.default.yml' if @mt_config[:use_postgres]

  gsub_file 'lib/tasks/cucumber.rake', "File.dirname(vendored_cucumber_bin) + '/../lib'", '"#{File.dirname(vendored_cucumber_bin)}/../lib"'
  lint_and_commit 'Add Cucumber'
end

def _fix_cucumber_task
  cucumber_task_file = 'lib/tasks/cucumber.rake'
  content            = File.read(cucumber_task_file)
  content.sub!('"#{Rails.root}/vendor/{gems,plugins}/cucumber*/bin/cucumber"', "Rails.root.join('vendor', '{gems,plugins}', 'cucumber*', 'bin', 'cucumber')") # rubocop:disable Lint/InterpolationCheck
  content.sub!('task :statsetup do', 'task statsetup: :environment do')
  content.sub!('task :annotations_setup do', 'task annotations_setup: :environment do')
  content.sub!("task 'test:prepare' do", "task 'test:prepare' => :environment do")
  content.sub!('task :cucumber do', 'task cucumber: :environment do')
  content.sub!('unless ARGV.any? {|a| a =~ /^gems/}', "unless ARGV.any? { |a| a.start_with? 'gems' }")
  file cucumber_task_file, force: true do
    content
  end
end

def _add_capybara_screenshot
  inject_into_file 'Gemfile', after: /gem 'capybara', '.*'/ do
    "\n  gem 'capybara-screenshot'"
  end
  bundle_install
  lint_and_commit 'Add Capybara-screenshot'
end

def _configure_cucumber
  copy_template ['features', 'support', 'configuration.rb']
  gsub_file 'features/support/env.rb', 'DatabaseCleaner.strategy = :transaction', 'DatabaseCleaner.strategy = :truncation' if @mt_config[:use_webpacker]
  lint_and_commit 'Create Cucumber configuration'
end

def setup_cucumber
  doc :test do
    <<~MD
      ### Cucumber - [site](https://github.com/cucumber/cucumber-rails)

      Cucumber is configured with
      [capybara-screenshot](http://github.com/mattheworiordan/capybara-screenshot),
      which makes HTML and png screenshots of pages when a step fails. Both HTML
      and images screenshots are saved in `tmp/capybara_screenshots`.

      By default, Cucumber will use Firefox to run the tests, but this can be
      changed with the `CAPYBARA_DRIVER` environment variable:

      ```sh
      # Default with firefox
      bundle exec cucumber
      # Variants
      CAPYBARA_DRIVER=firefox-headless bundle exec cucumber
      CAPYBARA_DRIVER=chrome bundle exec cucumber
      CAPYBARA_DRIVER=chrome-headless bundle exec cucumber
      ```

      When using Chrome/Chromium, Cucumber steps will fail on Javascript
      errors.

      The project uses the
      [webdrivers](https://github.com/titusfortner/webdrivers) gem, which
      manage the browsers respective drivers.
    MD
  end

  _add_cucumber

  copy_template ['cucumber.yml']
  lint_and_commit 'Randomize Cucumber tests'

  _add_capybara_screenshot
  _configure_cucumber
end

def setup_brakeman
  doc :test do
    <<~MD
      ### Brakeman - [site](https://brakemanscanner.org/)

      Brakeman is a "security scanner" for common mistakes leading to security
      issues.

      It can be launched with:

      ```sh
      bundle exec brakeman
      ```
    MD
  end

  inject_into_file 'Gemfile', after: @mt_config[:test_gems_separator] do
    "\n  gem 'brakeman'"
  end
  bundle_install
  lint_and_commit 'Add Brakeman'
end

def setup_simplecov
  doc :test do
    <<~MD
      ### Code coverage

      When using RSpec or Cucumber, code coverage summary is generated in
      `coverage/`. Don't hesitate to open it and check by yourself.
    MD
  end

  inject_into_file 'Gemfile', after: @mt_config[:dev_test_gems_separator] do
    "\n  gem 'simplecov', require: false"
  end
  # FIXME: CHANGE THIS
  # inject_into_file 'bin/rails', after: '#! /usr/bin/env ruby' do
  #   <<~'RUBY'
  #
  #     if ENV['RAILS_ENV'] == 'test'
  #       require 'simplecov'
  #       SimpleCov.start 'rails'
  #       puts 'required simplecov'
  #     end
  #   RUBY
  # end
  prepend_file 'spec/spec_helper.rb' do
    "\nrequire 'simplecov'\nSimpleCov.start 'rails'\n"
  end
  append_file '.gitignore' do
    "\n\n# Code coverage\ncoverage/\n"
  end
  bundle_install

  lint_and_commit 'Add Simplecov'
end
