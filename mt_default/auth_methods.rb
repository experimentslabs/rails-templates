# frozen_string_literal: true

def _add_devise # rubocop:disable Metrics/AbcSize
  inject_into_file 'Gemfile', after: @mt_config[:app_gems_separator] do
    "\ngem 'devise'"
  end
  bundle_install
  generate 'devise:install'
  generate 'devise', @mt_config[:auth_table]

  @mt_config[:auth_migration_file] = Dir.glob("db/migrate/*_devise_create_#{@mt_config[:auth_table]}.rb").first

  uncomment_lines @mt_config[:auth_migration_file], /.*confirm.*/

  copy_template ['spec', 'factories', 'users.rb'], "#{@mt_config[:auth_table]}.rb"

  inject_into_file @mt_config[:auth_model], after: ':recoverable, :rememberable, :validatable' do
    <<~RB


      scope :confirmed, -> { where.not(confirmed_at: nil) }

      def confirmed?
        confirmed_at.present?
      end
    RB
  end

  remove_file 'spec/models/user_spec.rb'

  lint_and_commit 'Add Devise and configure it'
end

def _set_devise_username
  inject_into_file 'app/controllers/application_controller.rb', after: /class ApplicationController < ActionController::(Base|API)/ do
    render_template ['controllers', 'application_controller_devise_username.rb']
  end

  inject_into_file @mt_config[:auth_migration_file], after: "t.string :encrypted_password, null: false, default: ''" do
    "\n    t.string :username,           null: false, default: nil"
  end

  inject_into_file @mt_config[:auth_migration_file], after: 'add_index :users, :email,                unique: true' do
    "\n    add_index :users, :username,             unique: true"
  end

  inject_into_file @mt_config[:auth_model], after: ':recoverable, :rememberable, :validatable' do
    "\n\nvalidates :username, presence: true, uniqueness: true, format: { with: /[a-z0-9\-_]{3,}/i }\n"
  end

  inject_into_file "spec/factories/#{@mt_config[:auth_table]}.rb", after: "password { 'password' }" do
    "\nusername { Faker::Internet.unique.username }\n"
  end

  lint_and_commit "Devise: Add username#{@mt_config[:use_roles] ? ' and role fields' : 'field'} to users"
end

def _add_devise_to_rspec
  copy_template ['spec', 'support', 'devise.rb']
  inject_into_file 'spec/rails_helper.rb', after: '# Add additional requires below this line. Rails is not loaded until this point!' do
    "\nrequire 'support/devise'\nrequire 'support/shared_contexts'"
  end

  copy_template ['spec', 'support', 'shared_contexts.rb']

  inject_into_file 'spec/rails_helper.rb', after: '# config.filter_gems_from_backtrace("gem name")' do
    <<~RB

      config.before(:suite) do
        FactoryBot.create :#{@mt_config[:auth_singular]}_known
      end
    RB
  end

  lint_and_commit 'Add Devise support to RSpec'
end

def _copy_devise_translations
  @mt_config[:locales].each { |locale| run "bundle exec rails generate devise:i18n:locale #{locale}" }
  lint_and_commit 'Add Devise translations'
end

def _copy_devise_views
  if @mt_config[:internationalize]
    run 'bundle exec rails generate devise:i18n:views '
  else
    run 'bundle exec rails generate devise:views '
  end
  lint_and_commit 'Add Devise views for future override'
end

def _add_devise_methods_to_acceptance_tests
  inject_into_file 'spec/support/devise.rb', before: 'end' do
    "  config.include DeviseAcceptanceSpecHelpers, type: :acceptance\n"
  end

  inject_into_file 'spec/support/devise.rb', before: 'RSpec.configure do |config|' do
    render_template ['spec', 'support', 'devise', 'acceptance.rb']
  end

  lint_and_commit 'RSpec Rails API: Make "sign_in" and "sign_out" accessible'
end

def setup_devise # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity
  doc :dev do
    factories = [
      "`#{@mt_config[:auth_singular]}` for a random user",
      "`#{@mt_config[:auth_singular]}_known` for an user with email \"user@example.com\"",
    ]
    factories += [
      "`#{@mt_config[:auth_singular]}_admin` for an admin",
      "`#{@mt_config[:auth_singular]}_admin_known` for an admin with email \"admin@example.com\"",
    ]

    <<~MD
      ### Authentication

      We use [Devise](https://github.com/plataformatec/devise) for
      authentication.

      These FactoryBot factories will help you during the development:

      - #{factories.join("\n- ")}

      All created #{@mt_config[:auth_table].singularize} are created with the `password` password unless
      you specify a custom one.
    MD
  end

  _add_devise
  _add_devise_i18n if @mt_config[:use_devise]
  _set_devise_username if @mt_config[:auth_username]
  _copy_devise_views if @mt_config[:devise_copy_views]
  _copy_devise_translations if @mt_config[:devise_copy_translations]
  _add_devise_to_rspec if @mt_config[:has_tests]
  _add_devise_methods_to_acceptance_tests if @mt_config[:use_rspec_rails_api]
end

def _add_pundit
  inject_into_file 'Gemfile', after: @mt_config[:app_gems_separator] do
    "\ngem 'pundit'"
  end

  inject_into_file 'app/controllers/application_controller.rb', after: /< ActionController::(Base|API)/ do
    "\ninclude Pundit"
  end

  if @mt_config[:auth_table] != 'users'
    inject_into_file 'app/controllers/application_controller.rb', after: 'private' do
      render_template ['controllers', 'application_controller_pundit_user.rb']
    end
  end

  bundle_install
  generate 'pundit:install'

  lint_and_commit 'Add Pundit'
end

def _pundit_add_roles
  inject_into_file "spec/factories/#{@mt_config[:auth_table]}.rb", before: /^ {2}end/ do
    render_template ['spec', 'factories', 'users', 'authorization.rb']
  end

  inject_into_file @mt_config[:auth_migration_file], before: '## Database authenticatable' do
    "## Authorization\nt.string :role, null: false, default: 'user'\n\n"
  end

  inject_into_file @mt_config[:auth_model], before: /^end/ do
    render_template ['models', 'user', 'authorization.rb']
  end

  lint_and_commit 'Users: Add role support'
end

def _pundit_copy_policies
  copy_template(['app', 'policies', 'application_policy.rb'])
  copy_template(['app', 'policies', 'member', 'member_application_policy.rb']) if config[:create_member_namespace]
  copy_template(['app', 'policies', 'admin', 'admin_application_policy.rb']) if config[:create_admin_namespace]

  lint_and_commit 'Pundit: Create default policies'
end

def _pundit_configure_rspec
  inject_into_file 'spec/rails_helper.rb', after: /FactoryBot.create :[a-z_]+_known/ do
    <<~RB

      # Controllers where users can create entities _should_ all be already protected by Devise "authenticate_user"
      ApplicationController.send(:after_action, :verify_authorized, except: [:index, :new, :create])
      ApplicationController.send(:after_action, :verify_policy_scoped, only: [:index])
    RB
  end

  lint_and_commit 'Pundit: Check authorization in RSpec tests'
end

def setup_pundit
  doc :dev do
    <<~MD
      ### Authorization

      Authorization is managed with
      [Pundit](https://github.com/varvet/pundit).

      Usage of `authorize xxx` and `policy_scope` are not enforced in
      controllers, but in RSpec tests (check `spec/rails_helper.rb`).
    MD
  end

  _add_pundit
  _pundit_add_roles if @mt_config[:use_roles]
  _pundit_copy_policies
  _pundit_configure_rspec
end

def setup_controller_namespaces
  doc :dev do
    namespaces = [
      '`app/controllers` is for public controllers',
    ]
    namespaces << '`app/controllers/member` is for registered users' if @mt_config[:create_member_namespace]
    namespaces << '`app/controllers/admin` is for admins' if @mt_config[:create_admin_namespace]

    <<~MD
      ### Roles namespaces

      Roles have their own controller namespaces (while models have not):

      - #{namespaces.join("\n- ")}
    MD
  end

  namespaces = []
  if @mt_config[:create_member_namespace]
    copy_template ['app', 'controllers', 'member', 'member_application_controller.rb']
    namespaces << 'member'
  end

  if @mt_config[:create_admin_namespace]
    copy_template ['app', 'controllers', 'admin', 'admin_application_controller.rb']
    namespaces << 'admin'
  end

  lint_and_commit "Create controller namespaces: #{namespaces.join(', ')}"
end
