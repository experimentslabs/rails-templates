# frozen_string_literal: true

def configure_database
  doc :setup do
    <<~MD
      ### Database configuration

      We use PostgreSQL for this project.

      To get started, copy `config/database.default.yml` to `config/database.yml`
      and modify the file to match your setup.

      ```sh
      cp config/database.default.yml config/database.yml
      ```

      Create database and run the migrations and seeds:

      ```sh
      bundle exec rails db:create
      bundle exec rails db:migrate
      bundle exec rails db:seed
      ```

      Drop the database:

      ```sh
      bundle exec rails db:drop
      ```
    MD
  end

  git rm: 'config/database.yml --cached'
  append_file '.gitignore' do
    <<~IGNORE

      # Database configuration (use the default as template)
      config/database.yml

    IGNORE
  end

  run 'cp config/database.yml config/database.default.yml'
  copy_template ['config', 'database.ci.yml']
  lint_and_commit 'Create a default database configuration', '.', false
end

def create_seeds
  doc :dev do
    <<~MD
      ### Seeds

      There are 3 seeds files available in the project:

      - `db/seeds_development.rb`
      - `db/seeds_production.rb`
      - `db/seeds.rb` for shared seeds.

      When you seed the database with `rails db:seed`, shared seeds are run
      first, then one of the other files is executed, depending on the
      environment.
    MD
  end

  append_file 'db/seeds.rb' do
    <<~'RUBY'
      Rails.logger.debug "Seeding for #{Rails.env}"
      require File.join(__dir__, 'seeds_production') if Rails.env.production?
      require File.join(__dir__, 'seeds_development') if Rails.env.development?
    RUBY
  end
  copy_template ['db', 'seeds_production.rb']
  copy_template ['db', 'seeds_development.rb']
  lint_and_commit 'Create development and production seeds', '.'
end
