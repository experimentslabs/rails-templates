# frozen_string_literal: true

def create_flash_messages
  copy_template ['app', 'views', 'layouts', '_flash_messages.html.haml']
  # This is ok, as the file will be converted to haml
  inject_into_file 'app/views/layouts/application.html.erb', before: '    <%= yield %>' do
    "    <%= render 'layouts/flash_messages' %>\n"
  end
end

def _add_haml_lint
  inject_into_file 'Gemfile', after: @mt_config[:test_gems_separator] do
    "\n  gem 'haml_lint', require: false"
  end
  bundle_install
  copy_template ['.haml-lint.yml']
  lint_and_commit 'Add Haml_lint'
end

def _add_haml_rails
  inject_into_file 'Gemfile', after: @mt_config[:app_gems_separator] do
    "\n  gem 'haml-rails'"
  end
  bundle_install
  lint_and_commit 'Add Haml-rails'
end

def _convert_erb
  run 'bundle exec rails haml:erb2haml', with: 'HAML_RAILS_DELETE_ERB=true'

  # At least, keep the ERB version of the text mailers
  git checkout: 'app/views/layouts/mailer.text.erb'
  remove_file 'app/views/layouts/mailer.text.haml'
  lint_and_commit 'Convert ERB views to HAML (except text mailers)'
end

# This is doable as we work on a small set of simple files
def _fix_haml_files
  Dir.glob('app/views/**/*.html.haml') do |file|
    # Layout/SpaceInsideHashLiteralBraces: Space inside { missing.
    # Layout/SpaceInsideHashLiteralBraces: Space inside } missing.
    gsub_file file, /([^#]){\s*(.*[^\s])\s*}/, '\1{ \2 }'
    # Style/StringLiterals: Prefer single-quoted strings when you don't need string interpolation or special symbols.
    gsub_file file, '"', "'"
  end

  # Discard changes in our file
  git checkout: 'app/views/layouts/_flash_messages.html.haml'

  gsub_file 'app/views/layouts/application.html.haml', "I18n.locale = '\\\#{I18n.locale}'", "I18n.locale = '\#{I18n.locale}'"
  gsub_file 'app/views/layouts/application.html.haml', "= javascript_pack_tag 'locales/\#{I18n.locale}'", '= javascript_pack_tag "locales/#{I18n.locale}"' # rubocop:disable Lint/InterpolationCheck

  lint_and_commit 'Fix Haml issues'
end

def setup_haml
  doc :dev do
    <<~MD
      ### Haml views - [site](http://haml.info/)

      This project uses HAML views, except for text emails.

      To help having a consistent formatting, `haml_lint` checks files in CI.

      To run it, simply execute:

      ```sh
      haml-lint
      ```
    MD
  end
  # Install Haml-lint first, as there are issues with haml version required by this
  # package and the one required by 'haml-rails'
  _add_haml_lint
  _add_haml_rails
  _convert_erb
  _fix_haml_files
end

def rescue_from_errors
  string = if @mt_config[:use_devise]
             'private'
           else
             /^end/
           end
  inject_into_file 'app/controllers/application_controller.rb', before: string do
    render_template ['controllers', 'application_controller_rescue_from.rb']
  end

  inject_into_file 'app/controllers/application_controller.rb', before: /^end/ do
    render_template ['controllers', 'application_controller_rescue_methods.rb']
  end
end
