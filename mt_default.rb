# frozen_string_literal: true

# Rails template
# ==============
# Author    : Manuel Tancoigne
# Repo      : https://gitlab.com/experimentslabs/rails-templates
# For rails : >= 5 and < 7
# State     : Used often, but use at your own risks :)
# Notes     : Meant to be used with Postgresql only.
#
# Simple Rails template which adds Devise (for User) and a few testing gems
#
# Contains:
# - A 'database.sample.yml' instead of 'database.yml'
# - A 'database.ci.yml' with configuration for Gitlab CI
# - Code sniffers:
#   - Rubocop with plugins (performance, rails and rspec)
#   - Haml lint
#   - Brakeman
# - Tests:
#   - Cucumber
#   - Rspec
# - Test tools:
#   - Capybara Screenshot
#   - Faker
#   - FactoryBot
#   - Simplecov
# - Haml templates
# - Devise with 'confirmable' behavior
# - Gitlab CI:
#   - Linting (Rubocop, Haml_lint, Factory_bot_lint)
#   - Static code analysis (Brakeman)
#   - Tests (RSpec, Cucumber)
#   - Outdated packages (gems and yarn packages)
#
# To disable Turbolinks, use the built-in "--skip_turbolinks" option
# To disable the test suite, use the built-in "--skip_test" option

require_relative 'mt_default/auth_methods'
require_relative 'mt_default/database_methods'
require_relative 'mt_default/file_methods'
require_relative 'mt_default/gitlab_methods'
require_relative 'mt_default/i18n_methods'
require_relative 'mt_default/methods'
require_relative 'mt_default/node_methods'
require_relative 'mt_default/tests_methods'
require_relative 'mt_default/views_methods'

raise "Unsupported version #{RAILS_VERSION}" unless rails_5? || rails_6?

lint_and_commit 'Prepare initial configuration', '.', false

# Preparation
@mt_config = question_user rails_options: @options

# Relax ruby version in Gemfile
relax_ruby_version

# Database configuration
configure_database if @mt_config[:use_postgres]

# =============================================================================
# Additional gems
# =====================================

setup_rubocop
if @mt_config[:has_tests]
  setup_database_cleaner
  setup_rspec
  setup_cucumber unless @mt_config[:is_api]
  setup_simplecov
end
setup_factorybot if @mt_config[:use_factorybot]
setup_brakeman

# FIXME: add Danger and configure it: https://danger.systems/ruby/
# FIXME: add Shiba and configure it: https://github.com/burrito-brothers/shiba

# A line in the gemfile below which new gems will be inserted
add_gemfile_separator

# =============================================================================
# Authentication
# =====================================

if @mt_config[:use_devise]
  setup_devise
  setup_pundit if @mt_config[:use_pundit]
  setup_controller_namespaces if @mt_config[:setup_controller_namespaces]
end

internationalize if @mt_config[:internationalize]

unless @mt_config[:is_api]
  create_flash_messages
  setup_haml
end

# if @mt_config[:is_api] && @mt_config[:use_devise]
# FIXME: Add devise-JWT and configure it when in API mode
# end

# =============================================================================
# Node modules
# =====================================

create_node_version

unless @mt_config[:is_api] || !@mt_config[:node_version]
  setup_eslint
  setup_stylelint
end

setup_vuejs if @mt_config[:use_vue]

# =============================================================================
# Styles and assets
# =====================================

convert_to_scss unless @mt_config[:is_api]

# =============================================================================
# Custom changes
# =====================================
rescue_from_errors

# =============================================================================
# File manipulations
# =====================================

# FIXME: Set root route
# FIXME: Disable auth token on json requests
configure_action_mailer
create_editorconfig
create_seeds

create_gitlab_ci
create_gitlab_templates

# FIXME: Create a README with useful infos about the app and setup
create_changelog if @mt_config[:create_changelog]
create_translations if @mt_config[:internationalize]

remove_dir 'test'
lint_and_commit 'Remove default tests', '.', false

# =============================================================================
# Summary
# =====================================

steps_to_do = [
  'Now what ?',
  '',
  'Connect to database',
  '  - Edit config/database.yml',
  '  - Run "bundle exec rails db:create"',
  '  - Run "bundle exec rails db:migrate"',
  '  - Run "bundle exec rails db:seed"',
  '',
  'Commit schema.rb',
  '',
  'Fix translations, maybe',
  '  - Run "bundle exec rails i18n:add-models-attributes"',
  '  - Run "bundle exec rails i18n:add-missing"',
  '  - Fix missing translations',
  '',
  'Fix HAML views (and make CI pass)',
]

if @mt_config[:use_webpacker]
  steps_to_do += [
    '',
    'Fix .gitignore: Webpacker duplicated some lines',
    '',
  ]
end

steps_to_do += [
  '',
  'Happy coding!',
]

after_bundle do # rubocop:disable Metrics/BlockLength
  FileUtils.mv 'app/javascript/packs/hello_vue.js', 'app/javascript/packs/hello_vue.js.sample' if @mt_config[:use_vue]
  lint_and_commit 'Complete initial setup'

  documentation = @doc.keys.map { |key| @doc[key].join("\n") }.join("\n\n")
  File.write 'README.md', documentation
  lint_and_commit 'Create README'

  cols = `/usr/bin/env tput cols`.to_i
  if cols >= 80
    fancy_lines = ''
    steps_to_do.each { |step| fancy_lines += fancy_line(step) }
    fancy_summary = <<~SUMMARY
      ╔═══════════════════╦══════════════════════════════════════════════════════════╗
      ║   ┬─┬             ║          Thank you for using this template!              ║
      ║   │°│ Experiments ║                    ^v^v^v^v^v^v^v^                       ║
      ║  /  .\\    Labs    ║    You can improve this template by contributing at      ║
      ║ (O____)           ║   https://gitlab.com/experimentslabs/rails-templates     ║
      ║                   ║                    ^v^v^v^v^v^v^v^                       ║
      ╠═[ Todo next ]═════╩══════════════════════════════════════════════════════════╣
      ║                                                                              ║#{fancy_lines}
      ║                                                                              ║
      ╚══════════════════════════════════════════════════════════════════════════════╝
    SUMMARY
    say fancy_summary, :green
  else
    say "\nTemplate from Experiments Labs\n", :green
    say "If you want to improve this it, merge requests are welcome at\n"
    say "https://gitlab.com/experimentslabs/rails-templates\n\n"
    say 'What you should do next:', :green
    say '========================', :green
    steps_to_do.each { |step| say step }
  end
end
