# Rails templates

> Collection of templates to use with the Rails application generator

**Note**: You need to clone this repo to have access to the templates.

## Templates

### `mt_default`

```rb
# Author    : Manuel Tancoigne
# Repo      : https://gitlab.com/experimentslabs/rails-templates
# For rails : >= 5 and < 7
# State     : Used often, but use at your own risks :)
# Notes     : Meant to be used with Postgresql only.
```

#### It provides

Some of the optional elements are asked when creating the project.

Auth:
- Devise (optional), configured for `User`
- Pundit (optional, using the `User.role` field, with `user` and `admin` roles)

Views
- Haml views (for non-API projects)
  - Haml-lint
- Views converted and cleaned templates (except text mailers): they pass Haml-lint

Tests
- Rspec-rails, configured
- Cucumber with:
  - Capybara screenshot
  - An experimental configuration to report JS errors (Chrome/chromium only)
  - Switch used browser with an environment variable
  - Selenium as default driver
- FactoryBot and Faker to generate content in tests
  - A FactoryBot-lint task to check if factories are replayable

Other
- An untracked database configuration with a sample file
- Simplecov
- `application.css` is now `application.scss` and don't require the world
- ActionMailer's default url set to `localhost:3000` for `test` and `development` environments
- An editorconfig file
- Changelog and Roadmap files (optional)

Linting/Static analysis
- Rubocop with extensions: performance, rails and rspec
- ESLint and StyleLint with their configuration (for non-API projects)
- Brakeman

Translations
- A custom default language
- A custom list of supported locales
- Rails-I18n with clean translation files
- i18n-tasks with additional tasks to
  - Generate model attributes
  - Add missing strings, prefixed with 'TRANSLATE_ME' for easy find
CI
- A Gitlab CI configuration file that runs:
  - Static code analysis
  - Tests
  - Shows outdated ruby and node dependencies

A commit is made after each step, and Rubocop autofix is ran before each commit...

#### Usage

```sh
# Rails 5:
rails new my_app -d postgresql -t path/to/mt_default.rb
# Rails 6:
rails new my_app -d postgresql -m path/to/mt_default.rb
```

## Contributing

You have a template to share ? Fork and create a merge request :)

Make sure it passes Rubocop tests

## Todo

- ~~Add a Gemfile with Rubocop dependency~~
- Enable CI
